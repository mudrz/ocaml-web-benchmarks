const path = require('path')

const fastify = require('fastify')({
  logger: false
})
const mustache = require('mustache')
const mopts = {
  partials: {
    header: 'header.mustache',
    footer: 'footer.mustache',
  }
}
fastify.register(require('point-of-view'), {
  engine: {
    mustache
  },
  templates: path.join(__dirname, 'templates'),
})

fastify.get('/', async (req, reply) => {
  return reply.view('welcome.mustache', {}, mopts)
})
fastify.get("/json", {
  schema: {
    response: {
      200: {
        type: 'object',
        properties: {
          message: { type: 'string' }
        }
      }
    }
  }
}, (req, reply) => {
  reply
    .header("Content-Type", "application/json")
    .code(200)
    .send({ message: "Hello, World!" });
});

const knex = require("knex")({
  client: "pg",
  connection: {
    host: "localhost",
    password: "",
    database: "opi"
  },
});

function excerptsByAuthor(author) {
  return knex("excerpts").select("*").where({ author });
}

fastify.get('/fortunes', async (req, reply) => {
  const fortunes = await excerptsByAuthor('kan')

  fortunes.push({ author: 'kan', excerpt: 'My excerpt', source: 'my source', page: '23' });
  fortunes.sort((a, b) => a.excerpt.localeCompare(b.excerpt));

  return reply.view('fortunes.mustache', { excerpts: fortunes }, mopts)
})

const EXCERPTS_SCHEMA = {
  "title": "Example Schema",
  "type": "object",
  "properties": {
    "excerpts": {
      "type": "array",
      "items": {
        "minimum": 0,
        "type": "object",
        "properties": {
          "author": { "type": "string" },
          "excerpt": { "type": "string" },
          "source": { "type": "string" },
          "page": { "type": "string" },
        },
        "additionalProperties": false,
        "required": ["author", "excerpt", "source"]
      },
    }
  },
  "additionalProperties": false,
  "required": ["excerpts"]
}

fastify.get('/fortunes-json', {
  schema: {
    response: {
      200: EXCERPTS_SCHEMA
    }
  }
}, async (req, reply) => {
  const fortunes = await excerptsByAuthor('kan')

  fortunes.push({ author: 'kan', excerpt: 'My excerpt', source: 'my source', page: '23' });
  fortunes.sort((a, b) => a.excerpt.localeCompare(b.excerpt));

  return reply
    .header("Content-Type", "application/json")
    .code(200)
    .send({ excerpts: fortunes });
})

fastify.get('/fortunes-json2', async (req, reply) => {
  const fortunes = await excerptsByAuthor('kan')

  fortunes.push({ author: 'kan', excerpt: 'My excerpt', source: 'my source', page: '23' });
  fortunes.sort((a, b) => a.excerpt.localeCompare(b.excerpt));

  return reply
    .header("Content-Type", "application/json")
    .code(200)
    .send({ excerpts: fortunes });
})

const start = async () => {
  try {
    const server = fastify.listen(3001)
    console.log('fastify listening on 3001')
    await server
  } catch (err) {
    fastify.log.error(err)
    process.exit(1)
  }
}
start()
