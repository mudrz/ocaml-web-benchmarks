open Opium.Std
open Tyxml
(* HTML generation library *)

(** The route handlers for our app *)

(** Defines a handler that replies to GET requests at the root endpoint *)
let root = get "/root"
    begin fun _req ->
      let res = Response.of_html Content.welcome_page in
      Lwt.return res
    end

(** Defines a handler that takes a path parameter from the route *)
let hello = get "/hello/:lang"
    begin fun req ->
      let lang = Router.param req "lang" in
      let res = Response.of_html (Content.hello_page lang) in
      Lwt.return res
    end

(** Fallback handler in case the endpoint is called without a language parameter *)
let hello_fallback = get "/hello"
    begin fun _req ->
      let res = Response.of_html (Content.basic_page Html.[p [txt "Hiya"]]) in
      Lwt.return res
    end

let get_excerpts_add = get "/excerpts/add"
    begin fun _req ->
      let res = Response.of_html Content.add_excerpt_page in
      Lwt.return res
    end

let respond_or_err resp = function
  | Ok v      -> Response.of_html (resp v)
  | Error err -> Response.of_html (Content.error_page err)
;;

let excerpt_of_form_data data =
  let find data key =
    let open Core in
    (* NOTE Should handle error in case of missing fields *)
    List.Assoc.find_exn ~equal:String.equal data key |> String.concat
  in
  let author  = find data "author"
  and excerpt = find data "excerpt"
  and source  = find data "source"
  and page    = match find data "page" with "" -> None | p -> Some p
  in
  Lwt.return Excerpt.{author; excerpt; source; page}
;;

let post_excerpts_add = post "/excerpts/add" begin fun req ->
    let open Lwt.Syntax in
    (* NOTE Should handle possible error arising from invalid data *)
    let* encoded = Request.to_urlencoded req in
    let* excerpt = excerpt_of_form_data encoded in
    let+ added = Db.Update.add_excerpt excerpt req in
    respond_or_err (fun () -> Content.excerpt_added_page excerpt) added
  end

let excerpts_by_author = get "/excerpts/author/:name" begin fun req ->
    let open Lwt.Syntax in
    let name = Router.param req "name" in
    let+ excerps = Db.Get.excerpts_by_author name req in
    respond_or_err Content.excerpts_listing_page excerps
  end

let get_excerpts req name =
  let open Lwt.Syntax in
  let* excerpts = Db.Get.excerpts_by_author name req in
  let excerpts = match excerpts with
    | Error e -> Error e
    | Ok xs -> 
      let x = Excerpt.{ author= "kan"; excerpt= "My excerpt"; source= "my source"; page= Some "23" } in
      let sorted = x::xs |> List.sort (fun a -> fun b -> String.compare a.Excerpt.excerpt b.excerpt) in
      Ok (sorted) in
  Lwt.return excerpts

let fortunes = get "/fortunes" begin fun req ->
    let open Lwt.Syntax in
    let* excerpts = get_excerpts req "kan" in
    Lwt.return (respond_or_err Content.excerpts_listing_page excerpts)
  end

let fortunes_json = get "/fortunes-json" begin fun req ->
    let open Lwt.Syntax in
    let* excerpts = get_excerpts req "kan" in
    let res = match excerpts with
      | Error e -> 
        let json = Excerpt.Response.Err.to_yojson { message= e } in
        Response.of_json json
      | Ok xs ->
        let json = Excerpt.Response.Ok.to_yojson { excerpts= xs } in
        Response.of_json json in
    Lwt.return res
  end

module Hello_world = struct
  type t =
    { hello: string
    }[@@deriving yojson]

  let hello_world = get "/" begin fun _req ->
      let res = Response.of_json (to_yojson { hello= "world" }) in
      Lwt.return res
    end
end

let excerpts = get "/excerpts" begin fun req ->
    let open Lwt.Syntax in
    let+ authors = Db.Get.authors req in
    respond_or_err Content.author_excerpts_page authors
  end

let routes =
  [ Hello_world.hello_world
  ; root
  ; hello
  ; hello_fallback
  ; excerpts
  ; get_excerpts_add
  ; post_excerpts_add
  ; excerpts_by_author
  ; excerpts
  ; fortunes
  ; fortunes_json
  ]

let add_routes app =
  Core.List.fold ~f:(fun app route -> route app) ~init:app routes
