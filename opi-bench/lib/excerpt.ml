type t =
  { author: string
  ; excerpt: string
  ; source: string
  ; page: string option
  }[@@deriving yojson]

module Response = struct
 module Ok = struct
  type excerpt = t[@@deriving yojson]
  type t = { excerpts: excerpt list
  }[@@deriving yojson]
 end
 module Err = struct
  type t =
   { message: string
   }[@@deriving yojson]
 end
end
