open Opium.Std

let static =
  Middleware.static
    ~local_path:"./static"
    ~uri_prefix:"/static"
    ()

let port = 4001
(** Build the Opium app  *)
let app : Opium.App.t =
  App.empty
  |> App.cmd_name "Ocaml Webapp Tutorial"
  |> App.port port
  |> middleware static
  |> Opi.Db.middleware
  |> Opi.Route.add_routes

let log_level = Some Logs.Debug

(** Configure the logger *)
let set_logger () =
  let reporter = Logs_fmt.reporter () in
  Logs.set_reporter reporter;
  Logs.set_level log_level

(** Run the application *)
let () =
  set_logger ();
  (* run_command' generates a CLI that configures a deferred run of the app *)
  match App.run_command' app with
  (* The deferred unit signals the deferred execution of the app *)
  | `Ok (app : unit Lwt.t ) -> 
    let _ = Lwt_io.printf "Running on port %d\n" port in
    Lwt_main.run app
  | `Error                  -> exit 1
  | `Not_running            -> exit 0
