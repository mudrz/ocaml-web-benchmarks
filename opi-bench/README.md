The first time:
  - run `make init` to create an opam switch and install all dependencies
  - (if the installation fails, run `make base`)
  - `make migrate`
