# fsharp-bench

A [Giraffe](https://github.com/giraffe-fsharp/Giraffe) web application, which has been created via the `dotnet new giraffe` command.

Requires https://dotnet.microsoft.com/download/dotnet-core/3.1
