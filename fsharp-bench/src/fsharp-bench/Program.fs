module fsharp_bench.App

open System
open System.IO
open Microsoft.AspNetCore.Builder
open Microsoft.AspNetCore.Cors.Infrastructure
open Microsoft.AspNetCore.Hosting
open Microsoft.Extensions.Hosting
open Microsoft.Extensions.Logging
open Microsoft.Extensions.DependencyInjection
open Giraffe
open FSharp.Control.Tasks.V2.ContextInsensitive

type hello_world = { hello: string }

let indexHandler: HttpHandler = json { hello = "world" }

let rootHandler: HttpHandler =
    let view = Content.welcome ()
    htmlView view

let fortunesHandler: HttpHandler =
    fun ctx next ->
        task {
            let! excerpts = Db.fortunes ()
            let view = Content.excerpts_listing_page excerpts
            return! htmlView view ctx next
        }

[<CLIMutable>]
type res = { excerpts: Excerpt.t list }

let fortunesJsonHandler: HttpHandler =
    fun ctx next ->
        task {
            let! excerpts = Db.fortunes ()
            let res = { excerpts = excerpts }
            return! json res ctx next
        }

let webApp =
    choose [ GET
             >=> choose [ route "/" >=> indexHandler
                          route "/root" >=> rootHandler
                          route "/fortunes" >=> fortunesHandler
                          route "/fortunes-json" >=> fortunesJsonHandler ]
             setStatusCode 404 >=> text "Not Found" ]

// ---------------------------------
// Error handler
// ---------------------------------

let errorHandler (ex: Exception) (logger: ILogger) =
    logger.LogError(ex, "An unhandled exception has occurred while executing the request.")

    clearResponse
    >=> setStatusCode 500
    >=> text ex.Message

// ---------------------------------
// Config and Main
// ---------------------------------

let configureCors (builder: CorsPolicyBuilder) =
    builder.WithOrigins("http://localhost:8080").AllowAnyMethod().AllowAnyHeader()
    |> ignore

let configureApp (app: IApplicationBuilder) =
    let env =
        app.ApplicationServices.GetService<IWebHostEnvironment>()

    (match env.EnvironmentName with
     | "Development" -> app.UseDeveloperExceptionPage()
     | _ -> app.UseGiraffeErrorHandler(errorHandler)).UseHttpsRedirection().UseCors(configureCors).UseStaticFiles()
        .UseGiraffe(webApp)

let configureServices (services: IServiceCollection) =
    services.AddCors() |> ignore
    services.AddGiraffe() |> ignore

let configureLogging (builder: ILoggingBuilder) =
    builder.AddFilter(fun l -> l.Equals LogLevel.Error).AddConsole().AddDebug()
    |> ignore

[<EntryPoint>]
let main args =
    let contentRoot = Directory.GetCurrentDirectory()
    let webRoot = Path.Combine(contentRoot, "WebRoot")
    Console.WriteLine("Starting...")
    let port = 5001
    let port = 3000
    let port = string port

    Host.CreateDefaultBuilder(args)
        .ConfigureWebHostDefaults(fun webHostBuilder ->
        webHostBuilder.UseContentRoot(contentRoot).UseWebRoot(webRoot).UseUrls("http://localhost:" + port)
                      .Configure(Action<IApplicationBuilder> configureApp).ConfigureServices(configureServices)
                      .ConfigureLogging(configureLogging)
        |> ignore

        Console.WriteLine("Giraffe listening on :" + port)).Build().Run()

    0
