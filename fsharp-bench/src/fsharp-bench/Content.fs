module fsharp_bench.Content

open Giraffe
open GiraffeViewEngine

let txt = encodedText

let layout (content: XmlNode list) =
    html [] [
        head [] [
            title [] [ txt "Fsharp Bench" ]
            meta [ _charset "UTF-8" ]
            link [ _rel  "stylesheet"
                   _type "text/css"
                   _href "/style.css" ]
        ]
        body [] content
    ]

let hyper_link children href = a [ _href href ] [ str children ]

let welcome () =
    [
        h1 [] [ txt "FSharp Webapp Tutorial" ]
        h2 [] [ txt "Hello" ]
        ul [] (List.map (fun x -> li [] [x]) [
              hyper_link "hiya" "/hello"
             ; hyper_link "‚Ä∞‚àè‚â†√ä√±√°" "/hello/‚Ä∞‚àè‚â†√ä√±√°"
             ; hyper_link "Deutsch" "/hello/Deutsch"
             ; hyper_link "English" "/hello/English"
             ])
        h2 [] [ txt "Excerpts" ]
        ul [] (List.map (fun x -> li [] [x]) [
              hyper_link "Add Excerpt" "/excerpts/add"
             ; hyper_link "Excerpts" "/excerpts"
             ])
    ] |> layout

let excerpt_elt (e : Excerpt.t) =
  let page = match e.page with
               | None -> ""
               | Some p -> Printf.sprintf ", %s" p
  in
  blockquote [_class "excerpt"] [
            p [][txt e.excerpt]
           ; p [][txt (Printf.sprintf "-- %s (%s%s)" e.author e.source page)]
  ]

let excerpts_listing_page (es : Excerpt.t list) =
    [
        h1 [][txt "Excerpts"]
        div [] (List.map excerpt_elt es)
    ] |> layout

