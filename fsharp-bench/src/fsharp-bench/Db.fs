module fsharp_bench.Db

open Dapper
open Npgsql
open FSharp.Control.Tasks.V2

let url = "localhost"
let port = 5432
let database = "opi"

let connection_uri =
    Printf.sprintf "Host=%s;Port=%i;Database=%s;Maximum Pool Size=%i" url port database 10

let inline (=>) a b = a, box b

let fortunes () =
    task {
        use conn = new NpgsqlConnection(connection_uri)

        let sql = """
      SELECT author, excerpt, source, page
      FROM excerpts
      WHERE author = @author;
   """
        let! data = conn.QueryAsync<Excerpt.t>(sql, dict [ "author" => "kan" ])
        let excerpts = List.ofSeq data

        let excerpt: Excerpt.t =
            { author = "kan"
              excerpt = "My excerpt"
              source = "my source"
              page = Some "23" }

        let sorted =
            excerpt
            :: excerpts
            |> List.sortBy (fun (a: Excerpt.t) -> a.excerpt)

        return sorted
    }
