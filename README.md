- Each dir has its own `Makefile` and `README.md`
- The naming convention is:
  - `make init` - run once initially
  - `make install` - install dependencies
  - `make run` - run the app (in dev mode)

Start with the OCaml dir since there are some steps present only there - the DB table creation (migrations)

You can then run:
html only:
```
wrk -t8 -c400 -d30s http://localhost:3001/
```
db access + html:
```
wrk -t8 -c400 -d30s http://localhost:3001/fortunes
```
db access + json:
```
wrk -t8 -c400 -d30s http://localhost:3001/fortunes-json
```

The ports are:
- 3001 - JS fastify
- 3002 - JS hapi
- 4001 - OCaml
- 5001 - F#
