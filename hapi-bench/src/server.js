const path = require('path')
const Hapi = require('@hapi/hapi');
const fastJson = require('fast-json-stringify')

const home = (req, reply) => {
  return reply.view('welcome.mustache')
}

const stringifyMessage = fastJson({
        type: 'object',
        properties: {
          message: { type: 'string' }
        }
      })

const handleJson = (_req, reply) => {
  const res = stringifyMessage({ message: "Hello, World!" });
  return reply
    .response(res)
    .header("Content-Type", "application/json")
}

const knex = require("knex")({
  client: "pg",
  connection: {
    host: "localhost",
    password: "",
    database: "opi"
  },
});

function excerptsByAuthor(author) {
  return knex("excerpts").select("*").where({ author });
}

const EXCERPTS_SCHEMA = {
  "title": "Example Schema",
  "type": "object",
  "properties": {
    "excerpts": {
      "type": "array",
      "items": {
        "minimum": 0,
        "type": "object",
        "properties": {
          "author": { "type": "string" },
          "excerpt": { "type": "string" },
          "source": { "type": "string" },
          "page": { "type": "string" },
        },
        "additionalProperties": false,
        "required": ["author", "excerpt", "source"]
      },
    }
  },
  "additionalProperties": false,
  "required": ["excerpts"]
}

const fortunesToString = fastJson(EXCERPTS_SCHEMA);

const fortunesJson = async (_req, reply) => {
  const fortunes = await excerptsByAuthor('kan')

  fortunes.push({ author: 'kan', excerpt: 'My excerpt', source: 'my source', page: '23' });
  fortunes.sort((a, b) => a.excerpt.localeCompare(b.excerpt));

  const res = fortunesToString({ excerpts: fortunes });
  return reply
    .response(res)
    .header("Content-Type", "application/json")
}
const fortunesJson2 = async (_req, _reply) => {
  const fortunes = await excerptsByAuthor('kan')

  fortunes.push({ author: 'kan', excerpt: 'My excerpt', source: 'my source', page: '23' });
  fortunes.sort((a, b) => a.excerpt.localeCompare(b.excerpt));

  return { excerpts: fortunes }
}

const fortunes = async (_req, reply) => {
  const fortunes = await excerptsByAuthor('kan')

  fortunes.push({ author: 'kan', excerpt: 'My excerpt', source: 'my source', page: '23' });
  fortunes.sort((a, b) => a.excerpt.localeCompare(b.excerpt));

  return reply.view('fortunes.mustache', { excerpts: fortunes })
}

const Mustache = require('mustache')
const start = async () => {
    const server = Hapi.server({
        port: 3002,
        host: 'localhost'
    });
    await server.register(require('@hapi/vision'));
    const partials = {};
    server.views({
        engines: {
            mustache: {
                compile: function (template) {
                    Mustache.parse(template);
                    return function (context) {
                        return Mustache.render(template, context, partials);
                    };
                },
                registerPartial: function (name, src) {
                    partials[name] = src;
                }
            }
        },
        relativeTo: __dirname,
        path: 'templates',
        partialsPath: 'templates',
    });

    server.route({
        method: 'GET',
        path: '/',
        handler: home
    });
    server.route({
        method: 'GET',
        path: '/json',
        handler: handleJson,
    });
    server.route({
        method: 'GET',
        path: '/fortunes-json',
        handler: fortunesJson,
    });
    server.route({
        method: 'GET',
        path: '/fortunes-json2',
        handler: fortunesJson2,
    });
    server.route({
        method: 'GET',
        path: '/fortunes',
        handler: fortunes,
    });

    await server.start();
    console.log('Server running on %s', server.info.uri);
}
process.on('unhandledRejection', (err) => {
    console.log(err);
    process.exit(1);
});
start()
